INSERT OVERWRITE TABLE LOCATION_VALUES  Select cast(regexp_replace(split(location_2,',')[0],'\\(','') as double),
	cast(regexp_replace(split(location_2,',')[1],'\\)','') as double),
	dr_no,
	2 * asin(
    sqrt(
      cos(radians(34.0667)) *
      cos(radians(cast(regexp_replace(split(location_2,',')[0],'\\(','') as double))) *
      pow(sin(radians((-118.1678 - cast(regexp_replace(split(location_2,',')[1],'\\)','') as double))/2)), 2)
          +
      pow(sin(radians((34.0667 - cast(regexp_replace(split(location_2,',')[0],'\\(','') as double))/2)), 2)

    )
  ) *3956,
  2 * asin(
    sqrt(
      cos(radians(34.0689)) *
      cos(radians(cast(regexp_replace(split(location_2,',')[0],'\\(','') as double))) *
      pow(sin(radians((-118.4452 - cast(regexp_replace(split(location_2,',')[1],'\\)','') as double))/2)), 2)
          +
      pow(sin(radians((34.0689 - cast(regexp_replace(split(location_2,',')[0],'\\(','') as double))/2)), 2)

    )
  ) *3956,
  2 * asin(
    sqrt(
      cos(radians(34.0205)) *
      cos(radians(cast(regexp_replace(split(location_2,',')[0],'\\(','') as double))) *
      pow(sin(radians((-118.2856 - cast(regexp_replace(split(location_2,',')[1],'\\)','') as double))/2)), 2)
          +
      pow(sin(radians((34.0205 - cast(regexp_replace(split(location_2,',')[0],'\\(','') as double))/2)), 2)

    )
  ) *3956
	from LAPD_CRIME_DATA;