create view csuladist as 
select location_values.distance_csula as Radius, count(*) as CSULA
from (

  select case  
    when distance_csula between 0 and 5 then '0-5'
    when distance_csula between 5 and 10 then '5-10'
	when distance_csula between 10 and 15 then '11-15'
	when distance_csula between 15 and 20 then '15-20'
	when distance_csula between 20 and 25 then '20-25'
	when distance_csula between 25 and 30 then '25-30'
	when distance_csula between 30 and 35 then '30-35'
	when distance_csula between 35 and 40 then '35-40'
	when distance_csula between 40 and 50 then '40-50' 
   else '>50'
    
	end as distance_csula
  from location_values) location_values
group by location_values.distance_csula 
order by radius;