create view uscdist as 
select location_values.distance_usc as Radius, count(*) as USC
from (

  select case  
    when distance_usc between 0 and 5 then '0-5'
    when distance_usc between 5 and 10 then '5-10'
	when distance_usc between 10 and 15 then '11-15'
	when distance_usc between 15 and 20 then '15-20'
	when distance_usc between 20 and 25 then '20-25'
	when distance_usc between 25 and 30 then '25-30'
	when distance_usc between 30 and 35 then '30-35'
	when distance_usc between 35 and 40 then '35-40'
	when distance_usc between 40 and 50 then '40-50' 
   else '>50'
    
	end as distance_usc
  from location_values) location_values
group by location_values.distance_usc 
order by radius;